package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import DB.ConnectionFactory;
import Entity.User;

public class UserDAO extends GenericDAO{

	public User getProfile(User u) {
		Logger.getLogger(DAO.UserDAO.class.getName()).log(Level.INFO, "Executing getProfile(" + u.toString() + ")");
		User user = null;
		try {
			String query = "SELECT * FROM USER WHERE USERID = ?";
			initConnection(query);
			statement.setString(1, u.getUserID());
			result = statement.executeQuery();
			while (result.next()) {
				user = new User(result.getString("USERID"), result.getString("LASTNAME"), result.getString("FIRSTNAME"),
						result.getString("BIRTHDAY"));
			}
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(DAO.UserDAO.class.getName()).log(Level.SEVERE, "Error retrieving user details", ex);
		}
		return user;
	}

	public boolean login(String username, String password) {
		Logger.getLogger(DAO.UserDAO.class.getName()).log(Level.INFO,
				"Executing login(userid=" + username + ", password=" + password + ")");
		boolean isValid = false;
		try {
			String query = "SELECT COUNT(*) 'COUNT' FROM USER WHERE USERID = ? AND PASSWORD = ?";
			initConnection(query);
			statement.setString(1,  username);
			statement.setString(2,  password);
			result = statement.executeQuery();
			while (result.next()) {
				if (result.getInt("COUNT") > 0)
					isValid = true;
			}

		} catch (SQLException ex) {
			Logger.getLogger(DAO.UserDAO.class.getName()).log(Level.SEVERE, "Error retrieving user details", ex);
		}
		return isValid;
	}

}
