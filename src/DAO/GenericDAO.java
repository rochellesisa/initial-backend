package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DB.ConnectionFactory;

public class GenericDAO {
	
	Connection connection;
	PreparedStatement statement;
	ResultSet result;
	ConnectionFactory myFactory;

	public void initConnection(String query) throws SQLException {
		myFactory = ConnectionFactory.getInstance();
		connection = myFactory.getConnection();
		statement = connection.prepareStatement(query);
	}

}
