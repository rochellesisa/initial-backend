package DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Entity.Glossary;

public class GlossaryDAO extends GenericDAO {

	public List<Glossary> getGlossary() {
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO, "Executing getGlossary()");
		List<Glossary> glossary = new ArrayList<>();
		try {
			String query = "SELECT * FROM GLOSSARY";
			initConnection(query);
			result = statement.executeQuery();
			while (result.next()) {
				glossary.add(
						new Glossary(result.getInt("ID"), result.getString("TERM"), result.getString("DEFINITION")));
			}
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error adding to history", ex);
		}
		return glossary;
	}
}
