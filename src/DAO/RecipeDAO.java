package DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Entity.Ingredient;
import Entity.Recipe;
import Entity.Steps;

public class RecipeDAO extends GenericDAO{
	
	public Recipe getRecipeDetails(int recipeId){
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO,
				"Executing getRecipeDetails(" + recipeId + ")");
		Recipe recipe = null;
		try {
			String query = "SELECT * FROM RECIPE WHERE ID = ?";
			initConnection(query);
			statement.setInt(1, recipeId);
			result = statement.executeQuery();
			while (result.next()) {
				recipe = new Recipe(result.getInt("ID"),result.getString("TITLE"),result.getString("DESCRIPTION"),result.getInt("YIELD"),result.getString("SOURCE"),result.getString("PHOTO"));
			}
			connection.close();
			
			recipe.setIngredients(getRecipeIngredients(recipeId));
			recipe.setSteps(getRecipeSteps(recipeId));
			
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error retrieving recipe details", ex);
		}
		return recipe;
	}
	
	public List<Ingredient> getRecipeIngredients(int recipeId){
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO,
				"Executing getRecipeIngredients(" + recipeId + ")");
		List<Ingredient> ingredients = new ArrayList<>();
		try {
			String query = "SELECT NAME, MEASURE, UNIT, DESCRIPTION FROM view_recipe_ingredients WHERE ID = ?";
			initConnection(query);
			statement.setInt(1, recipeId);
			result = statement.executeQuery();
			while (result.next()) {
				Ingredient in = new Ingredient();
				in.setName(result.getString("NAME"));
				in.setMeasure(result.getInt("MEASURE"));
				in.setUnit(result.getString("UNIT"));
				in.setDescription(result.getString("DESCRIPTION"));
				ingredients.add(in);
			}
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error retrieving recipe ingredients", ex);
		}
		return ingredients;
	}
	
	public List<Steps> getRecipeSteps(int recipeId){
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO,
				"Executing getRecipeSteps(" + recipeId + ")");
		List<Steps> steps = new ArrayList<>();
		try {
			String query = "SELECT * FROM STEPS WHERE RECIPEID = ?"; 
			initConnection(query);
			statement.setInt(1, recipeId);
			result = statement.executeQuery();
			while (result.next()) {
				Steps s = new Steps();
				s.setStepNumber(result.getInt("STEPNUMBER"));
				s.setDescription(result.getString("DESCRIPTION"));
				s.setTimer(result.getInt("TIMER"));
				steps.add(s);
			}
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error retrieving recipe steps", ex);
		}
		return steps;
	}
	
	public List<Recipe> searchRecipeResult(List<String> keywords){
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO,
				"Executing searchRecipeResult(keysize=" + keywords.size() + ")");
		List<Recipe> recipeList = new ArrayList<>();
		List<Integer> idList = new ArrayList<>();
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT ID FROM VIEW_CONCAT_KEYWORD");
			if (!keywords.isEmpty()){
				sb.append(" WHERE ");
				for (int x = 0; x < keywords.size(); x++){
					sb.append("TEXT LIKE '%" + keywords.get(x) + "%' ");
					sb.append((x == keywords.size()-1) ? " OR " : "" );
				}
			}
			
			initConnection(sb.toString());
			result = statement.executeQuery();
			while (result.next()) {
				idList.add(result.getInt("ID"));
			}
			connection.close();
			
			for (int id : idList){
				recipeList.add(getRecipeDetails(id));
			}
			
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error retrieving recipe search results", ex);
		}
		return recipeList;
	}
	

}
