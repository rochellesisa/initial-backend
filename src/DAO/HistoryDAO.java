package DAO;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Entity.History;
import Entity.Recipe;
import Entity.User;

public class HistoryDAO extends GenericDAO {

	public void addToHistory(History h) {
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO,
				"Executing addToSearchHistory(" + h.toString() + ")");
		try {
			String query = "INSERT INTO HISTORY (USERID, RECIPEID, TYPE, TIMESTAMP) VALUES (?, ?, ?, ?)";
			initConnection(query);
			statement.setString(1, h.getUser().getUserID());
			statement.setInt(2, h.getRecipe().getRecipeId());
			statement.setInt(3, h.getType());
			statement.setTimestamp(4,  new Timestamp(System.currentTimeMillis()));
			statement.executeUpdate();
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error adding to history", ex);
		}
	}
	
	public void deleteFromHistory(History h) {
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO,
				"Executing deleteFromHistory(" + h.toString() + ")");
		try {
			String query = "DELETE FROM HISTORY WHERE USERID = ? AND RECIPEID = ? AND TYPE = ?";
			initConnection(query);
			statement.setString(1, h.getUser().getUserID());
			statement.setInt(2, h.getRecipe().getRecipeId());
			statement.setInt(3, h.getType());
			statement.executeUpdate();
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error deleting from history", ex);
		}
	}

	//0 = search	1 = cook	2 = saved
	public List<History> getHistory(User u, int historyFlag) {
		Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.INFO,
				"Executing getHistory(" + u.getUserID() + ")");
		List<History> searchHistory = new ArrayList<>();
		try {
			String query = "SELECT * FROM VIEW_HISTORY WHERE USERID = ? AND TYPE = ?";
			initConnection(query);
			statement.setString(1, u.getUserID());
			statement.setInt(2, historyFlag);
			result = statement.executeQuery();
			while (result.next()) {
				History h = new History();
				Recipe r = new Recipe();
				h.setUser(u);
				r.setRecipeId(result.getInt("ID"));
				r.setTitle(result.getString("TITLE"));
				r.setDescription(result.getString("DESCRIPTION"));
				r.setYield(result.getInt("YIELD"));
				r.setSource(result.getString("SOURCE"));
				r.setPhoto(result.getString("PHOTO"));
				h.setRecipe(r);
				h.setType(historyFlag);
				h.setTimestamp(result.getTimestamp("TIMESTAMP"));
				searchHistory.add(h);
			}
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(DAO.GenericDAO.class.getName()).log(Level.SEVERE, "Error retrieving search history", ex);
		}
		return searchHistory;
	}
	
}
