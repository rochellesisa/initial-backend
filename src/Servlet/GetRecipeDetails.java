package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import DAO.HistoryDAO;
import DAO.RecipeDAO;
import Entity.History;
import Entity.Recipe;
import Entity.User;

/**
 * Servlet implementation class GetRecipeDetails
 */
@WebServlet(description = "Retrieve recipe details", urlPatterns = { "/GetRecipeDetails" })
public class GetRecipeDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		HttpSession session = request.getSession();
		RecipeDAO recipeDao = new RecipeDAO();
		HistoryDAO historyDao = new HistoryDAO();
		PrintWriter out = response.getWriter();
		try {
			ServletContext context = getServletContext();
			RequestDispatcher dispatch = context.getRequestDispatcher("/view");
			User user = (User) session.getAttribute("user");
			Recipe recipe = recipeDao.getRecipeDetails(Integer.parseInt(request.getParameter("recipeid")));
			String json = new Gson().toJson(recipe);
			request.setAttribute("recipe", recipe);
			request.setAttribute("recipe", json);
			out.write(json);
			List<History> searchHistory = historyDao.getHistory(user, 0);		//0 = search	1 = cook	2 = saved
			for (History s : searchHistory){
				if(s.getRecipe().getRecipeId() == recipe.getRecipeId()){
					historyDao.deleteFromHistory(s);
					break;
				}
			}
			historyDao.addToHistory(new History(user, recipe, 0));
			
			dispatch.forward(request, response);
			System.out.println(json);
		} catch (Exception e) {
			Logger.getLogger(Servlet.GetUserProfile.class.getName()).log(Level.SEVERE, "Servlet error", e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}
