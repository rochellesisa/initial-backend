package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import DAO.GlossaryDAO;
import DAO.HistoryDAO;
import Entity.Glossary;
import Entity.History;
import Entity.User;

/**
 * Servlet implementation class GetGlossary
 */
@WebServlet("/GetGlossary")
public class GetGlossary extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		GlossaryDAO glossaryDao = new GlossaryDAO();
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
		try {
			ServletContext context = getServletContext();
			RequestDispatcher dispatch = context.getRequestDispatcher("/view");
			List<Glossary> glossaryList = glossaryDao.getGlossary();

			String json = new Gson().toJson(glossaryList);
			request.setAttribute("glossary", glossaryList);
			request.setAttribute("glossaryjson", json);
			out.write(json);
			dispatch.forward(request, response);
			 System.out.println(json);
			 
		} catch (Exception e) {
//			Logger.getLogger(Servlet.Login.class.getName()).log(Level.SEVERE, "Servlet error", e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}
