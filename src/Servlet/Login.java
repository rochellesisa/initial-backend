package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import DAO.UserDAO;
import Entity.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		response.setContentType("text/html;charset=UTF-8");
		UserDAO userDao = new UserDAO();
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
		try {
			ServletContext context = getServletContext();
			RequestDispatcher dispatch = context.getRequestDispatcher("/view");
			User user = null;
			if (userDao.login(request.getParameter("userid"), request.getParameter("password"))){
				user = new User();
				user.setUserID(request.getParameter("userid"));
				user = userDao.getProfile(user);
			}
			
			String json = new Gson().toJson(user);
			request.setAttribute("user", user);
			request.setAttribute("user", user);
			session.setAttribute("user", user);
			out.write(json);
			dispatch.forward(request, response);
			System.out.println(json);
		} catch (Exception e) {
			Logger.getLogger(Servlet.Login.class.getName()).log(Level.SEVERE, "Servlet error", e);
		} finally {
			out.close();
		}
	}

//	@Override
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		processRequest(request, response);
//	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}
