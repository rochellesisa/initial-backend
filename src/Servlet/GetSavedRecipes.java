package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import DAO.HistoryDAO;
import Entity.History;
import Entity.User;

/**
 * Servlet implementation class GetSavedHistory
 */
@WebServlet(description = "Retrieve user's saved recipes", urlPatterns = { "/GetSavedHistory" })
public class GetSavedRecipes extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		HistoryDAO historyDao = new HistoryDAO();
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
		try {
			ServletContext context = getServletContext();
			RequestDispatcher dispatch = context.getRequestDispatcher("/view");
			User user = (User) session.getAttribute("user");
			List<History> savedRecipe = historyDao.getHistory(user, 2);		//0 = search	1 = cook	2 = saved
			String json = new Gson().toJson(savedRecipe);
			request.setAttribute("savedrecipe", savedRecipe);
			request.setAttribute("savedrecipe", json);
			out.write(json);
			dispatch.forward(request, response);
			System.out.println(savedRecipe.size());
			System.out.println(json);
		} catch (Exception e) {
			Logger.getLogger(Servlet.GetUserProfile.class.getName()).log(Level.SEVERE, "Servlet error", e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}
