package Entity;

import java.util.List;

public class Recipe {
	private int recipeId;
	private String title;
	private String description;
	private int yield;
	private String source;
	private String photo;
	private List<Ingredient> ingredients;
	private List<Steps> steps;

	public Recipe() {
	}

	public Recipe(int recipeId, String title, String description, int yield, String source, String photo) {
		this.recipeId = recipeId;
		this.title = title;
		this.description = description;
		this.yield = yield;
		this.source = source;
		this.photo = photo;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public List<Steps> getSteps() {
		return steps;
	}

	public void setSteps(List<Steps> steps) {
		this.steps = steps;
	}

	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYield() {
		return yield;
	}

	public void setYield(int yield) {
		this.yield = yield;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "[recipeId=" + recipeId + ", title=" + title + "]";
	}

}
