package Entity;

public class User {

	private String userID;
	private String password;
	private String lastName;
	private String firstName;
	private String birthDay;

	public User() {
	}

	public User(String userID, String lastName, String firstName, String birthDay) {
		this.userID = userID;
		this.lastName = lastName;
		this.firstName = firstName;
		this.birthDay = birthDay;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	@Override
	public String toString() {
		return "[userId=" + userID + ", firstName=" + firstName + ", lastName=" + lastName + ", birthday=" + birthDay
				+ "]";
	}
}
