package Entity;

public class Glossary {
	private int glossaryId;
	private String term;
	private String definition;

	public Glossary() {
	}

	public Glossary(int glossaryId, String term, String definition) {
		this.glossaryId = glossaryId;
		this.term = term;
		this.definition = definition;
	}

	public int getGlossaryId() {
		return glossaryId;
	}

	public void setGlossaryId(int glossaryId) {
		this.glossaryId = glossaryId;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getdefinition() {
		return definition;
	}

	public void setdefinition(String definition) {
		this.definition = definition;
	}

}
