package Entity;

public class Ingredient {
	private String ingredientId;
	private String name;
	private IngredientType type;
	private int measure;
	private String unit;
	private String description;

	public Ingredient() {
	}

	public Ingredient(String ingredientId, String name, int measure, String unit, String description) {
		this.ingredientId = ingredientId;
		this.name = name;
		this.measure = measure;
		this.unit = unit;
		this.description = description;
	}

	public String getIngredientId() {
		return ingredientId;
	}

	public void setIngredientId(String ingredientId) {
		this.ingredientId = ingredientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IngredientType getType() {
		return type;
	}

	public void setType(IngredientType type) {
		this.type = type;
	}

	public int getMeasure() {
		return measure;
	}

	public void setMeasure(int measure) {
		this.measure = measure;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
