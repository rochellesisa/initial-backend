package Entity;

import java.sql.Timestamp;

public class History {
	private User user;
	private Recipe recipe;
	private int type;
	private Timestamp timestamp;

	public History() {
	}

	public History(User user, Recipe recipe, int type) {
		this.user = user;
		this.recipe = recipe;
		this.type = type;
	}

	public History(User user, Recipe recipe, int type, Timestamp timestamp) {
		this.user = user;
		this.recipe = recipe;
		this.type = type;
		this.timestamp = timestamp;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		String typeTxt = "";

		switch (type) {
		case 0:
			typeTxt = "search";
			break;
		case 1:
			typeTxt = "cooked";
			break;
		case 2:
			typeTxt = "saved";
			break;
		}

		return "[userId=" + user.getUserID() + ", recipeId=" + recipe.getRecipeId() + ", type=" + typeTxt + "]";
	}
}
