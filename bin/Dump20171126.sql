-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: recipeapp
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `glossary`
--

DROP TABLE IF EXISTS `glossary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glossary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TERM` varchar(50) NOT NULL,
  `DEFINITION` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glossary`
--

LOCK TABLES `glossary` WRITE;
/*!40000 ALTER TABLE `glossary` DISABLE KEYS */;
INSERT INTO `glossary` VALUES (1,'Al dente','Pasta cooked until just firm. From the Italian \"to the tooth.\"'),(2,'Bake','To cook food in an oven, surrounded with dry heat; called roasting when applied to meat or poultry'),(3,'Baking powder','A combination of baking soda, an acid such as cream of tartar, and a starch or flour (moisture absorber). Most common type is double-acting baking powder, which acts when mixed with liquid and again when heated.'),(4,'Barbecue','To cook foods on a rack or a spit over coals.'),(5,'Baste','To moisten food for added flavor and to prevent drying out while cooking.');
/*!40000 ALTER TABLE `glossary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `USERID` varchar(70) NOT NULL,
  `RECIPEID` int(11) NOT NULL,
  `TYPE` tinyint(4) NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USERID`,`RECIPEID`,`TYPE`),
  KEY `fk_USER_has_RECIPE_RECIPE1_idx` (`RECIPEID`),
  KEY `fk_USER_has_RECIPE_USER_idx` (`USERID`),
  CONSTRAINT `fk_USER_has_RECIPE_RECIPE1` FOREIGN KEY (`RECIPEID`) REFERENCES `recipe` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_USER_has_RECIPE_USER` FOREIGN KEY (`USERID`) REFERENCES `user` (`USERID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES ('johndoe@gmail.com',1,0,'2017-11-25 14:50:00'),('johndoe@gmail.com',2,0,'2017-11-25 16:59:05'),('johndoe@gmail.com',2,1,'2017-11-25 14:50:00'),('johndoe@gmail.com',3,0,'2017-11-25 14:50:00'),('johndoe@gmail.com',3,2,'2017-11-25 14:50:00');
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `TYPE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_INGREDIENT_INGREDIENT_TYPE1_idx` (`TYPE`),
  CONSTRAINT `fk_INGREDIENT_INGREDIENT_TYPE1` FOREIGN KEY (`TYPE`) REFERENCES `ingredient_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES (1,'Chicken',1),(2,'Lamb',1),(3,'Beef',1),(4,'Veal',1),(5,'Pork',1),(6,'Bacon',1),(7,'Ham',1),(8,'Hotdog',1),(9,'Sausage',1),(10,'Fish',1),(11,'Cauliflower',2),(12,'Broccoli',2),(13,'Spinach',2),(14,'Cucumber',2),(15,'Tomato',2),(16,'Garlic',2),(17,'Onion',2),(18,'Taro',2),(19,'Green Chili Pepper',2),(20,'Radish',2),(21,'String beans',2),(22,'Eggplant',2),(23,'Kankong',2),(24,'Fish Sauce',6),(25,'Tamarind Powder',9),(26,'Rice',8),(27,'Water',10);
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient_type`
--

DROP TABLE IF EXISTS `ingredient_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient_type`
--

LOCK TABLES `ingredient_type` WRITE;
/*!40000 ALTER TABLE `ingredient_type` DISABLE KEYS */;
INSERT INTO `ingredient_type` VALUES (1,'Meat'),(2,'Vegetable'),(3,'Fruit'),(4,'Condiment'),(5,'Herb'),(6,'Sauce'),(7,'Starch'),(8,'Grain'),(9,'Flavor Enhancers'),(10,'Liquid');
/*!40000 ALTER TABLE `ingredient_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `USERID` varchar(10) NOT NULL,
  `NAME` varchar(45) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  KEY `fk_INVENTORY_USER1_idx` (`USERID`),
  KEY `fk_INVENTORY_INGREDIENT_TYPE1_idx` (`TYPE`),
  CONSTRAINT `fk_INVENTORY_INGREDIENT_TYPE1` FOREIGN KEY (`TYPE`) REFERENCES `ingredient_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_INVENTORY_USER1` FOREIGN KEY (`USERID`) REFERENCES `user` (`USERID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(200) NOT NULL,
  `DESCRIPTION` text,
  `YIELD` int(11) DEFAULT NULL,
  `SOURCE` text,
  `PHOTO` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,'Pork Sinigang','a traditional Filipino soup dish known for its sour flavor.',5,'http://www.myfilipinorecipes.com/pork-sinigang','1porksinigang.jpg'),(2,'Buffalo Chicken Wings','Buffalo chicken wings are fried chicken wings coated with a hot and tangy sauce',4,'http://panlasangpinoy.com/2011/05/26/buffalo-chicken-wings/','2buffalowings.jpg'),(3,'Slow Roasted Pork Belly','Slow roasted pork belly',4,'https://www.gordonramsay.com/gr/recipes/slow-roasted-pork-belly/','3porkbelly.jpg');
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe_ingredient`
--

DROP TABLE IF EXISTS `recipe_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe_ingredient` (
  `RECIPE` int(11) NOT NULL,
  `INGREDIENT` int(11) NOT NULL,
  `MEASURE` varchar(10) DEFAULT NULL,
  `UNIT` varchar(45) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RECIPE`,`INGREDIENT`),
  KEY `fk_RECIPE_has_INGREDIENT_INGREDIENT1_idx` (`INGREDIENT`),
  KEY `fk_RECIPE_has_INGREDIENT_RECIPE1_idx` (`RECIPE`),
  CONSTRAINT `fk_RECIPE_has_INGREDIENT_INGREDIENT1` FOREIGN KEY (`INGREDIENT`) REFERENCES `ingredient` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_RECIPE_has_INGREDIENT_RECIPE1` FOREIGN KEY (`RECIPE`) REFERENCES `recipe` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe_ingredient`
--

LOCK TABLES `recipe_ingredient` WRITE;
/*!40000 ALTER TABLE `recipe_ingredient` DISABLE KEYS */;
INSERT INTO `recipe_ingredient` VALUES (1,5,'2','lbs','cut 2 inches chunks'),(1,15,'1',NULL,'quartered'),(1,17,'1',NULL,'quartered'),(1,18,'1',NULL,'quartered'),(1,19,'3',NULL,NULL),(1,20,'1',NULL,'Sliced'),(1,23,'10','stems',NULL),(1,24,NULL,NULL,NULL),(1,25,'1','pack',NULL),(1,27,NULL,NULL,NULL),(3,5,'1','kg',NULL),(3,16,'3','cloves','peeled and bashed');
/*!40000 ALTER TABLE `recipe_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `steps`
--

DROP TABLE IF EXISTS `steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `steps` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `STEPNUMBER` int(11) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `TIMER` int(11) DEFAULT NULL,
  `RECIPEID` int(11) NOT NULL,
  PRIMARY KEY (`ID`,`RECIPEID`),
  KEY `fk_STEPS_RECIPE1_idx` (`RECIPEID`),
  CONSTRAINT `fk_STEPS_RECIPE1` FOREIGN KEY (`RECIPEID`) REFERENCES `recipe` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `steps`
--

LOCK TABLES `steps` WRITE;
/*!40000 ALTER TABLE `steps` DISABLE KEYS */;
INSERT INTO `steps` VALUES (1,1,'Boil water in a pot',NULL,1),(2,2,'Add the pork liempo, tomato, onion, and tamarind sinigang soup mix.',NULL,1),(3,3,'Cover the pot and boil until the meat is tender.',NULL,1),(4,4,'Add the long chili pepper, radish, taro, string beans, and eggplant.',NULL,1),(5,5,'Cover the pot and continue boiling until the vegetables are cooked.',NULL,1),(6,6,'Add the kangkong leaves and simmer until the dish is done.',NULL,1),(7,7,'Serve while hot!',NULL,1);
/*!40000 ALTER TABLE `steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USERID` varchar(70) NOT NULL,
  `PASSWORD` varchar(45) NOT NULL,
  `LASTNAME` varchar(45) DEFAULT NULL,
  `FIRSTNAME` varchar(45) DEFAULT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  PRIMARY KEY (`USERID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('alexjames@hotmail.com','password','Alexander','James','1995-10-11'),('janedoe@yahoo.com','password','Doe','Jane','1990-03-16'),('johndoe@gmail.com','password','Doe','John','1994-10-30');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view_concat_keyword`
--

DROP TABLE IF EXISTS `view_concat_keyword`;
/*!50001 DROP VIEW IF EXISTS `view_concat_keyword`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_concat_keyword` AS SELECT 
 1 AS `ID`,
 1 AS `TEXT`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_history`
--

DROP TABLE IF EXISTS `view_history`;
/*!50001 DROP VIEW IF EXISTS `view_history`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_history` AS SELECT 
 1 AS `USERID`,
 1 AS `TYPE`,
 1 AS `ID`,
 1 AS `TITLE`,
 1 AS `DESCRIPTION`,
 1 AS `YIELD`,
 1 AS `SOURCE`,
 1 AS `PHOTO`,
 1 AS `TIMESTAMP`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_recipe_ingredients`
--

DROP TABLE IF EXISTS `view_recipe_ingredients`;
/*!50001 DROP VIEW IF EXISTS `view_recipe_ingredients`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_recipe_ingredients` AS SELECT 
 1 AS `ID`,
 1 AS `TITLE`,
 1 AS `RECIPEDESC`,
 1 AS `YIELD`,
 1 AS `SOURCE`,
 1 AS `PHOTO`,
 1 AS `NAME`,
 1 AS `MEASURE`,
 1 AS `UNIT`,
 1 AS `DESCRIPTION`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_concat_keyword`
--

/*!50001 DROP VIEW IF EXISTS `view_concat_keyword`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_concat_keyword` AS select `r`.`ID` AS `ID`,concat(concat(`r`.`TITLE`,`r`.`DESCRIPTION`),ifnull((select group_concat(`vi`.`NAME` separator ' ') from `view_recipe_ingredients` `vi` where (`vi`.`ID` = `r`.`ID`)),'')) AS `TEXT` from `recipe` `r` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_history`
--

/*!50001 DROP VIEW IF EXISTS `view_history`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_history` AS select `h`.`USERID` AS `USERID`,`h`.`TYPE` AS `TYPE`,`r`.`ID` AS `ID`,`r`.`TITLE` AS `TITLE`,`r`.`DESCRIPTION` AS `DESCRIPTION`,`r`.`YIELD` AS `YIELD`,`r`.`SOURCE` AS `SOURCE`,`r`.`PHOTO` AS `PHOTO`,`h`.`TIMESTAMP` AS `TIMESTAMP` from (`history` `h` join `recipe` `r` on((`r`.`ID` = `h`.`RECIPEID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_recipe_ingredients`
--

/*!50001 DROP VIEW IF EXISTS `view_recipe_ingredients`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_recipe_ingredients` AS select `r`.`ID` AS `ID`,`r`.`TITLE` AS `TITLE`,`r`.`DESCRIPTION` AS `RECIPEDESC`,`r`.`YIELD` AS `YIELD`,`r`.`SOURCE` AS `SOURCE`,`r`.`PHOTO` AS `PHOTO`,`i`.`NAME` AS `NAME`,`ri`.`MEASURE` AS `MEASURE`,`ri`.`UNIT` AS `UNIT`,`ri`.`DESCRIPTION` AS `DESCRIPTION` from ((`recipe` `r` join `recipe_ingredient` `ri` on((`r`.`ID` = `ri`.`RECIPE`))) join `ingredient` `i` on((`ri`.`INGREDIENT` = `i`.`ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-26  1:00:19
